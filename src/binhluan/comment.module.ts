import { Module } from '@nestjs/common';

import { JwtModule } from '@nestjs/jwt';

import { CommentService } from './comment.service';
import { CommentController } from './comment.controller';
import { JwtStrategy } from 'src/strategy/jwt.strategy';

@Module({
    imports: [JwtModule.register({})],
    controllers: [CommentController],
    providers: [CommentService, JwtStrategy],
})
export class CommentModule { }
