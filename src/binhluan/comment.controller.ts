import { Body, Controller, Get, Headers, Post, UseInterceptors, UploadedFile, UseGuards, Req, HttpException, HttpCode, Res, Param, Patch, Put, Delete } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiHeader, ApiParam, ApiProperty, ApiTags } from '@nestjs/swagger';
import { CommentService } from './comment.service';
import { Response } from 'express';
import { AuthGuard } from '@nestjs/passport';


class BodyApp3 {
    // @ApiProperty()
    // ma_cong_viec: number
    // @ApiProperty()
    // ma_nguoi_binh_luan: number
    @ApiProperty()
    ngay_binh_luan: Date
    @ApiProperty()
    noi_dung: string
    @ApiProperty()
    sao_binh_luan: number

}

@ApiTags("BinhLuan")

@Controller('comment')
export class CommentController {
    constructor(
        private readonly commentService: CommentService,
        private configService: ConfigService
    ) { }



    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt")) // lock api
    @HttpCode(200)
    @Get('/binh-luan')
    getComment() {
        return this.commentService.getComment();
    }

    @HttpCode(200)
    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt")) // lock api
    @Post('/binh-luan/:ma_cong_viec')
    postCommnet(@Body() reqBody: BodyApp3, @Res() res: Response, @Param("ma_cong_viec") ma_cong_viec: number, @Req() req: Request, @Headers("token") token) {


        this.commentService.postComment(reqBody, res, ma_cong_viec, req, token);
        res.send('Comment posted!!');


    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt")) // lock api
    @HttpCode(200)
    @Put('/binh-luan/:id')
    putComment(@Body() reqBody: BodyApp3, @Res() res: Response, @Req() req: Request, @Param("id") id: string) {
        this.commentService.putComment(reqBody, res, req, id);
        res.send("Update successfully")
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt")) // lock api
    @HttpCode(200)
    @Delete('/binh-luan/:id')
    deleteComment(@Res() res: Response, @Req() req: Request, @Param("id") id: string) {
        this.commentService.deleteComment(res, req, id);
        res.send("Delete successfully!!")
    }


    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt")) // lock api
    @HttpCode(200)
    @Get('/binh-luan/lay-binh-luan-theo-cong-viec/:ma_cong_viec')
    getCommentByJob(@Res() res: Response, @Req() req: Request, @Param("ma_cong_viec") ma_cong_viec: string) {
        this.commentService.getCommentByJob(res, req, ma_cong_viec);
        res.send(" successfully!!")

    }

}
