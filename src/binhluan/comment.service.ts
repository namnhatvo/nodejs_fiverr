import { JwtService } from '@nestjs/jwt';
import { Headers, Injectable, Param, Res, Req, Body } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { Response } from 'express';



@Injectable()
export class CommentService {

    constructor(
        private jwtService: JwtService
    ) { }

    prisma = new PrismaClient();

    async getComment() {
        let data = await this.prisma.binhLuan.findMany();

        return data;
    }

    async postComment(@Body() reqBody: any, @Res() res: Response, ma_cong_viec: number, @Req() req: Request, @Headers("token") token) {
        let { noi_dung, sao_binh_luan } = reqBody;



        let userInfo = this.jwtService.decode(token)


        await this.prisma.binhLuan.create({
            data: {
                ma_cong_viec: ma_cong_viec,
                ma_nguoi_binh_luan: userInfo.data.checkEmail.id,
                ngay_binh_luan: new Date(),
                noi_dung,
                sao_binh_luan
            }
        })


    }

    async putComment(@Body() reqBody: any, @Res() res: Response, @Req() req: Request, @Param("id") id: string) {
        let { noi_dung, sao_binh_luan } = reqBody;

        let commentId = parseInt(id, 10);

        let commentInfo = await this.prisma.binhLuan.findFirst({
            where: {
                id: commentId
            }
        })

        commentInfo = { ...commentInfo, ngay_binh_luan: new Date(), noi_dung, sao_binh_luan }

        await this.prisma.binhLuan.update({
            data: commentInfo, where: {
                id: commentId
            }
        })

        res.send("Update successfully!!")

    }


    async deleteComment(@Res() res: Response, @Req() req: Request, @Param("id") id: string) {
        let commentId = parseInt(id, 10);

        let data = await this.prisma.binhLuan.delete({
            where: {
                id: commentId
            }
        });

        return data;
    }

    async getCommentByJob(@Res() res: Response, @Req() req: Request, @Param("ma_cong_viec") ma_cong_viec: string) {

        console.log('ma_cong_viec:', ma_cong_viec);

        let jobId = parseInt(ma_cong_viec, 10);

        let data = await this.prisma.binhLuan.findMany({
            include: {
                NguoiDung: true
            },
            where: {
                ma_cong_viec: jobId
            }
        });
        console.log('Data:', data);
        return data;

    }

}
