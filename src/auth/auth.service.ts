import { Injectable, Body, Res } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { Prisma, PrismaClient } from "@prisma/client";
import * as bcrypt from "bcrypt"
import { error } from "console";
import { Response } from 'express';

@Injectable()
export class AuthService {

    constructor(
        private jwtService: JwtService
    ) { }

    prisma = new PrismaClient();


    async signUp(reqBody: any, @Res() res: Response) {
        let { name, email, pass_word, phone, birth_day, gender, role, skill, certification } = reqBody;

        let checkEmail = await this.prisma.nguoiDung.findFirst({
            where: {
                email: email
            }
        });

        if (checkEmail) {
            throw new Error('Email already existed');
        }

        let passCrypt = bcrypt.hashSync(pass_word, 10);

        await this.prisma.nguoiDung.create({
            data: {
                name,
                email,
                pass_word: passCrypt,
                phone,
                birth_day,
                gender,
                role: "user",
                skill,
                certification,
            }
        });
    }

    async login(reqBody: any, @Res() res: Response) {
        let { email, pass_word } = reqBody;

        let checkEmail = await this.prisma.nguoiDung.findFirst({
            where: {
                email: email
            }
        });

        if (checkEmail) {

            let checkPass = bcrypt.compareSync(pass_word, checkEmail.pass_word);

            if (checkPass) {
                let token = this.jwtService.sign({ data: { checkEmail, pass_word: '' } }, { secret: 'BIMAT', expiresIn: '5y' });
                res.send(token);
            } else {
                res.send("Wrong password");
            }
        } else {
            res.send("Wrong email");
        }
    }



}