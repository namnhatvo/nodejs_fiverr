import { Body, Controller, HttpCode, HttpException, NotFoundException, Post, Res } from '@nestjs/common';
import { AuthService } from './auth.service';
import { ApiProperty, ApiTags } from '@nestjs/swagger';
import { ConfigService } from '@nestjs/config';
import { Response } from 'express';


class BodyApp1 {

    @ApiProperty()
    name: string
    @ApiProperty()
    email: string
    @ApiProperty()
    pass_word: string
    @ApiProperty()
    phone: string
    @ApiProperty()
    birthday: string
    @ApiProperty()
    gender: string
    @ApiProperty()
    role: string
    @ApiProperty()
    skill: string
    @ApiProperty()
    certification: string
}
class BodyApp2 {
    @ApiProperty()
    email: string
    @ApiProperty()
    pass_word: string
}

@ApiTags("Auth")
@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService,
        private configService: ConfigService
    ) { }

    @HttpCode(200)
    @Post("/sign-up")
    singUp(@Body() reqBody: BodyApp1, @Res() res: Response) {
        try {
            this.authService.signUp(reqBody, res);
            res.send("Signup successfully!")
        } catch (error) {
            console.error(error.message);
            res.status(400).send(error.message);
        }
    }

    @Post("/login")
    login(@Body() reqBody: BodyApp2, @Res() res: Response) {
        try {
            return this.authService.login(reqBody, res);
        } catch (error) {
            console.error(error.message);
            res.status(400).send(error.message);
        }
    }
}
