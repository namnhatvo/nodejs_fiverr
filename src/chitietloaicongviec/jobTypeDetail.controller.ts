import { ConfigService } from '@nestjs/config';
import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put, Req, Res, UseGuards } from "@nestjs/common";
import { ApiBearerAuth, ApiProperty, ApiTags } from "@nestjs/swagger";
import { JobTypeDetailService } from './jobTypeDetail.service';
import { AuthGuard } from '@nestjs/passport';
import { Response } from 'express';


class BodyApp4 {
    @ApiProperty()
    ten_chi_tiet: string
    @ApiProperty()
    hinh_anh: string
}


@ApiTags("ChiTietLoaiCongViec")

@Controller('jobTypeDetail')
export class JobTypeDetailController {
    constructor(
        private readonly jobTypeDetailService: JobTypeDetailService,
        private ConfigService: ConfigService
    ) { }

    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt")) // lock api
    @HttpCode(200)
    @Get('/chi-tiet-loai-cong-viec')
    getJobTypeDetail() {
        return this.jobTypeDetailService.getJobTypeDetail();
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt")) // lock api
    @HttpCode(200)
    @Post('/chi-tiet-loai-cong-viec/:ma_loai_cong_viec')
    postJobTypeDetail(@Body() reqBody: BodyApp4, @Res() res: Response, @Param("ma_loai_cong_viec") ma_loai_cong_viec: string, @Req() req: Request) {
        this.jobTypeDetailService.postJobTypeDetail(reqBody, res, ma_loai_cong_viec, req);
        res.send('Posted!!');
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt")) // lock api
    @HttpCode(200)
    @Get('/chi-tiet-loai-cong-viec/phan-trang-tim-kiem')
    pagination() {

    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt")) // lock api
    @HttpCode(200)
    @Get('/chi-tiet-loai-cong-viec/:id')
    getJobTypeDetailById(@Res() res: Response, @Req() req: Request, @Param("id") id: string) {
        this.jobTypeDetailService.getJobTypeDetailById(res, req, id)
        res.send("Successfully!!")

    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt")) // lock api
    @HttpCode(200)
    @Put('/chi-tiet-loai-cong-viec/:id')
    putJobTypeDetail(@Body() reqBody: BodyApp4, @Res() res: Response, @Req() req: Request, @Param("id") id: string) {
        this.jobTypeDetailService.putJobTypeDetail(reqBody, res, req, id);
        res.send("Update successfully")

    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard("jwt")) // lock api
    @HttpCode(200)
    @Delete('/chi-tiet-loai-cong-viec/:id')
    deleteJobTypeDetail(@Res() res: Response, @Req() req: Request, @Param("id") id: string) {
        this.jobTypeDetailService.deleteJobTypeDetail(res, req, id)
        res.send("Delete successfully!!")
    }

}