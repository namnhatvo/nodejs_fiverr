import { Module } from '@nestjs/common';

import { JwtModule } from '@nestjs/jwt';

import { JwtStrategy } from 'src/strategy/jwt.strategy';
import { JobTypeDetailController } from './jobTypeDetail.controller';
import { JobTypeDetailService } from './jobTypeDetail.service';

@Module({
    imports: [JwtModule.register({})],
    controllers: [JobTypeDetailController],
    providers: [JobTypeDetailService, JwtStrategy],
})
export class JobTypeDetailModule { }
