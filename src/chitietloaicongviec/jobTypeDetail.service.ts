import { JwtService } from '@nestjs/jwt';

import { Body, Injectable, Param, Req, Res } from "@nestjs/common";
import { PrismaClient } from '@prisma/client';
import { Response } from 'express';


@Injectable()
export class JobTypeDetailService {
    constructor(
        private jwtService: JwtService
    ) { }

    prisma = new PrismaClient();

    async getJobTypeDetail() {
        let data = await this.prisma.chiTietLoaiCongViec.findMany();

        return data;
    }

    async postJobTypeDetail(@Body() reqBody: any, @Res() res: Response, @Param("ma_loai_cong_viec") ma_loai_cong_viec: string, @Req() req: Request) {
        let { ten_chi_tiet, hinh_anh } = reqBody

        let jobTypeId = parseInt(ma_loai_cong_viec, 10)

        await this.prisma.chiTietLoaiCongViec.create({
            data: {
                ten_chi_tiet: ten_chi_tiet,
                hinh_anh: hinh_anh,
                ma_loai_cong_viec: jobTypeId
            }
        })
    }

    async pagination() { }

    async getJobTypeDetailById(@Res() res: Response, @Req() req: Request, @Param("id") id: string) {
        let jobTypeDetailId = parseInt(id, 10);

        let data = await this.prisma.chiTietLoaiCongViec.findFirst({
            where: {
                id: jobTypeDetailId
            }
        });

        console.log('Data:', data);
        return data;

    }

    async putJobTypeDetail(@Body() reqBody: any, @Res() res: Response, @Req() req: Request, @Param("id") id: string) {
        let { ten_chi_tiet, hinh_anh } = reqBody

        let jobTypeDetailId = parseInt(id, 10);

        let typeInfo = await this.prisma.chiTietLoaiCongViec.findFirst({
            where: {
                id: jobTypeDetailId
            }
        })

        typeInfo = { ...typeInfo, ten_chi_tiet, hinh_anh }

        await this.prisma.chiTietLoaiCongViec.update({
            data: typeInfo, where: {
                id: jobTypeDetailId
            }
        })

        res.send("Update successfully!!")
    }

    async deleteJobTypeDetail(@Res() res: Response, @Req() req: Request, @Param("id") id: string) {
        let jobTypeDetailId = parseInt(id, 10);

        let data = await this.prisma.chiTietLoaiCongViec.delete({
            where: {
                id: jobTypeDetailId
            }
        });

        return data;

    }
}