import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './auth/auth.module';
import { CommentModule } from './binhluan/comment.module';
import { JobTypeDetailModule } from './chitietloaicongviec/jobTypeDetail.module';
@Module({
  imports: [AuthModule, CommentModule, JobTypeDetailModule, ConfigModule.forRoot({ isGlobal: true })],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
